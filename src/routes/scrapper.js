const express = require('express');
const router = express.Router();
const scrapper = require('../service/scrapper.js');

/* Fetch events from the facebook pages and save them into a json file */
router.post('/', function (req, res, next) {
  const fbPagesUrl = req.body.url;
  console.log('will fetch the pages : ', fbPagesUrl);

  scrapper(fbPagesUrl)
    .then((events) => {
      res.json(events);
    })
    .catch(err => {
      console.log(err);
      res.json('error with the file generation');
    });
});

module.exports = router;
