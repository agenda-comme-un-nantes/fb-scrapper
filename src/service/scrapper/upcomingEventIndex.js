const request = require('request-promise');

function fetch(fbPageId) {
  return request({
    uri: 'https://www.facebook.com/api/graphql/',
    method: 'POST',
    form: {
      av: '0',
      '__user=': '0',
      '__a': '1',
      '__dyn': '7AgNe5Gmawgrolg9obHG8GEW8xd4Wo5OdwJCwAxu13wmEW4UJe9xK5WAx-bxWUW16xq2WdxK4ohx3wCwxxicw8258e8hwj82oG3i0wpE9UbU8ofHG3q5U4m12wRyU-3K1tx278-0JUhwKK6Uy22225o-cBK1Uy82Yxa5bjy8aEaoGqfwl8aoy13wLwBgK7ojz47Ea8b82kwLwKG2q4U6-3OfxW68hwno4S',
      '__req': '5',
      '__be': '1',
      'dpr': 1,
      '__rev': '1000701786',
      '__s': 'vhyoma:t30drp',
      'lsd': 'AVrHHBUX',
      'jazoest': '2648',
      'fb_api_caller_class': 'RelayModern',
      'fb_api_req_friendly_name':	'PageEventsTabUpcomingEventsCardRendererQuery',
      'variables': `{"pageID":"${fbPageId}","count":30}`,
      'doc_id': '2455863461165494'
    },
    headers: {
      'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0',
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  });
}

function parse(graphqlAnswer) {
  const json = JSON.parse(graphqlAnswer);
  const events = json.data.page.upcoming_events.edges;
  return events.map(edge => edge.node.id);
}

async function scrap(fbPageId) {
  const graphqlAnswer = await fetch(fbPageId);
  const eventIds = parse(graphqlAnswer);

  return eventIds;
}

module.exports = {
  scrap
};
