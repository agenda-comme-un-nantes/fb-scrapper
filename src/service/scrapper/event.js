const request = require('request-promise');
const cheerio = require('cheerio');
const _ = require('lodash');

function fetch(url) {
  return request({
    uri: url,
    method: 'GET',
    headers: {
      'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0'
    }
  });
}

function fetchDescription(eventId) {
  return request({
    uri: `https://www.facebook.com/api/graphql/`,
    method: 'POST',
    form: {
      av: '0',
      '__user': '0',
      '__a': '1',
      '__dyn': '5V8WXBzamaUmgDBzFHpUR1ycCzScybGiWF3ozGFuS-CGgjK2a5RzoaqhEpyAubGqKi5azppEG5VGwwyKbG4V9B88x2axuF98SmjBXDmEgF3ebBz998iGtxifG6u8AzoSbBWAhfypfh6bx25UCiajz8gzAcy4mEepoG9J4xeumuibBDAzocooUkCKq9CJ4gqz8ixbAJkUGrxjDUG6aJUhxR5zopAgSUCdyFE-5oV6x6WLGFEHAxpu9iFkF7Giumqyboyut9wxk-8wDAyXyUooKn8FESbwgUgUK8BAyKqdrLJ164oBeEWbAzAtbhpUHAUGuq9xlki9hppbgjyUizpp8HBBzAiUhDzAqucyEy4p9VEycGdxOeGFUO8x6UCEPK',
      '__req': '2',
      '__be': '-1',
      '__pc': 'PHASED%3ADEFAULT',
      '__rev': '3896064',
      'lsd': 'AVowGSkE',
      'fb_api_caller_class': 'RelayModern',
      'variables': `{"eventID":"${eventId}"}`,
      'doc_id': '1640160956043533'
    },
    headers: {
      'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0'
    }
  });
}

function parse(htmlPage) {
  // facebook put some important information in HTMl comments
  const regexp = /<code id=.*<!--(.*?)--><\/code>/g;
  const uncommentedHtml = htmlPage.replace(regexp, '$1');
  const $ = cheerio.load(uncommentedHtml);

  const title = $('#event_header_primary h1').text();
  const htmlDates = $('._2ycp').attr('content');
  const hostsElement = $('div[data-testid=event_permalink_feature_line]');
  const primaryHost = hostsElement.children('a').first().text();
  const otherHostsRaw = hostsElement.children('a[data-hover=tooltip]').attr('data-tooltip-content');
  const otherHosts = otherHostsRaw ? otherHostsRaw.split("\n") : null;

  const dates = {
    start: null,
    end: null
  };

  if (htmlDates) {
    [dates.start, dates.end] = htmlDates.split(' to ');
  } else {
    console.log({id, message: 'no dates', htmlDates});
  }

  const image = [{
    value: $('[rel=theater] .scaledImageFitHeight,.scaledImageFitWidth').attr('src'),
    alt: ''
  }];

  return {
    title,
    start: dates.start,
    end: dates.end,
    place: "", // can't extract it easily
    hosts: _.union([primaryHost], otherHosts),
    image
  }
}

function parseDescription(graphqlAnswer) {
  const json = JSON.parse(graphqlAnswer);
  return json.data.event.details.text;
}

async function scrap(eventId) {
  const url = `https://www.facebook.com/events/${eventId}/`;
  const htmlPage = await fetch(url);
  const descriptionResult = await fetchDescription(eventId);

  const parsedData = parse(htmlPage);
  const description = parseDescription(descriptionResult);

  const event = {
    id: 'fb-' + eventId,
    url: url,
    ...parsedData,
    description: description.substring(0,200) + (description.length > 200 ? '...' : '')
  };

  return event;
}

module.exports = {
  scrap
};
