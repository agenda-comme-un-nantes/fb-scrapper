const request = require('request-promise');

function fetch(url) {
  return request({
    uri: url,
    method: 'GET',
    headers: {
      'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:59.0) Gecko/20100101 Firefox/59.0'
    }
  });
}

function parse(htmlPage) {
  // facebook put some important information in HTMl comments
  const titleRegexp = /<meta property="og:title" content="(.*?)" \/>/;
  const [, title] = htmlPage.match(titleRegexp);

  const idRegexp = /<meta property="al:android:url" content="fb:\/\/page\/(.*)\?referrer=app_link" \/>/;
  const [, id] = htmlPage.match(idRegexp);

  return {
    title,
    id
  }
}

async function scrap(url) {
  const htmlPage = await fetch(url);
  const parsedValues = parse(htmlPage);

  return {
    url,
    ...parsedValues
  };
}

module.exports = {
  scrap
};
