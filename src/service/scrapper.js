const promisify = require('util').promisify;
const fs = require('fs');
const writeFile = promisify(fs.writeFile);
const indexScrapper = require('./scrapper/eventIndex');
const upcomingEventsScrapper = require('./scrapper/upcomingEventIndex');
const recurringEventsScrapper = require('./scrapper/recurringEventIndex');
const eventScrapper = require('./scrapper/event');

async function saveInFile(events) {
  return writeFile('public/events.json', JSON.stringify(events));
}

async function extractEventsFromFbPage(url) {
  console.log('extract from : ', url);
  const fbPage = await indexScrapper.scrap(url);

  const sourceKeys = [{
    key: fbPage.url,
    libelle: fbPage.title
  }];
  const upcomingEventIds = await upcomingEventsScrapper.scrap(fbPage.id);
  const recurringEventIds = await recurringEventsScrapper.scrap(fbPage.id);
  const events = await Promise.all(
    upcomingEventIds.concat(recurringEventIds).map(async (id) => eventScrapper.scrap(id))
  );

  console.log('# ', url);
  return events.map((event) => {
    return {
      ...event,
      sourceKeys
    };
  });
}

function flatten(array) {
  return array.reduce((acc, val) => acc.concat(val), []);
}

function uniqEvents(events) {
  const reduced = events.reduce((acc, value) => {
    const id = value.id;
    if (acc[id]) {
      acc[id].sourceKeys = acc[id].sourceKeys.concat(value.sourceKeys);
    } else {
      acc[id] = value;
    }

    return acc;
  }, {});

  return Object.values(reduced);
}

module.exports = async (fbPagesUrl) => {
  const events = uniqEvents(flatten(await Promise.all(
    fbPagesUrl.map(extractEventsFromFbPage)
  )));

  await saveInFile(events);

  return events;
};
